import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ADMIN_ROUTES } from './admin.routing';
import { AdminComponentsModule } from '../../admin-components/admin-components.module';
import { AdminDashboardComponent } from '../../pages/admin-dashboard/admin-dashboard.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AdminDashboardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AdminComponentsModule,
    RouterModule.forChild( ADMIN_ROUTES )
   ],
  exports: [],
  providers: [],
})
export class AdminLayoutModule {}
