import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GAME_ROUTES } from './game.routes';
import { GameComponent } from '../../pages/game/game.component';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    GameComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    RouterModule.forChild( GAME_ROUTES )
  ],
  exports: [],
  providers: [],
})
export class GameLayoutModule {}
