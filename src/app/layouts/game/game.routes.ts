import { Routes } from '@angular/router';

import { GameComponent } from '../../pages/game/game.component';

export const GAME_ROUTES: Routes = [
  { path: 'game', component: GameComponent },

];

