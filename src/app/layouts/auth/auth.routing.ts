import { Routes } from '@angular/router';

import { LoginComponent } from '../../pages/login/login.component';
import { AdminLoginComponent } from '../../pages/admin-login/admin-login.component';

export const ROUTES_AUTH: Routes = [
  { path: 'game', component: LoginComponent },
  { path: 'admin', component: AdminLoginComponent }


];

