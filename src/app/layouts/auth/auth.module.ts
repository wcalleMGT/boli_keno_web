import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ROUTES_AUTH } from './auth.routing';
import { LoginComponent } from '../../pages/login/login.component';
import { AdminLoginComponent } from '../../pages/admin-login/admin-login.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    LoginComponent,
    AdminLoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild( ROUTES_AUTH )
  ],
  exports: [],
  providers: [],
})
export class AuthLayoutModule {}
