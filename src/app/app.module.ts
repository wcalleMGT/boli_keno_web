import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthComponent } from './layouts/auth/auth.component';
import { GameComponent } from './layouts/game/game.component';
import { ComponentsModule } from './components/components.module';
import { AdminComponent } from './layouts/admin/admin.component';
import { AdminComponentsModule } from './admin-components/admin-components.module';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    GameComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComponentsModule,
    AdminComponentsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
