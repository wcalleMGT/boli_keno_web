import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthComponent } from './layouts/auth/auth.component';
import { GameComponent } from './layouts/game/game.component';
import { AdminComponent } from './layouts/admin/admin.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', redirectTo: 'game', pathMatch: 'full' },
  { path: 'login',
    component: AuthComponent,
    loadChildren: './layouts/auth/auth.module#AuthLayoutModule' },
  {
    path: '',
    component: GameComponent,
    loadChildren: './layouts/game/game.module#GameLayoutModule'
  },
  {
    path: 'admin',
    component: AdminComponent,
    loadChildren: './layouts/admin/admin.module#AdminLayoutModule'
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
