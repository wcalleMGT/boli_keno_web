import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JackpotComponent } from './jackpot/jackpot.component';
import { StremingComponent } from './streming/streming.component';



@NgModule({
  declarations: [
    JackpotComponent,
    StremingComponent
  ],
  exports: [
    JackpotComponent,
    StremingComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
