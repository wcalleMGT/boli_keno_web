import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jackpot',
  templateUrl: './jackpot.component.html',
  styleUrls: ['./jackpot.component.css']
})
export class JackpotComponent implements OnInit {

  amountJackpot = 0;
  titulo: string;

  constructor() {}

  ngOnInit() {
    this.amountJackpot = 150;
    this.titulo = 'Jackpot';
    // console.log('iniciando jackpot');
  }

}
